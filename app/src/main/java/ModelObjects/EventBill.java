package ModelObjects;

/**
 * Created by Vaibhav on 4/28/2015.
 */
public class EventBill {

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBillDescription() {
        return billDescription;
    }

    public void setBillDescription(String billDescription) {
        this.billDescription = billDescription;
    }

    public Double getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(Double amountSpent) {
        this.amountSpent = amountSpent;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    private String eventId;
    private String userId;
    private String billDescription;
    private Double amountSpent;
    private String billDate;
    private String isApproved;

    public String getUniqueObjectId() {
        return uniqueObjectId;
    }

    public void setUniqueObjectId(String uniqueObjectId) {
        this.uniqueObjectId = uniqueObjectId;
    }

    private String uniqueObjectId;

    public boolean isBillApproved() {
        return isBillApproved;
    }

    public void setBillApproved(boolean isBillApproved) {
        this.isBillApproved = isBillApproved;
    }

    private boolean isBillApproved;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }




    private String billId;



}
