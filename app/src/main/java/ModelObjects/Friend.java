package ModelObjects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Niveditha on 4/20/2015.
 */

public class Friend{


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private boolean member;
    private boolean treasurer;


    private String grp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId;
    public boolean isTreasurer() {
        return treasurer;
    }

    public void setTreasurer(boolean treasurer) {
        this.treasurer = treasurer;
    }

    public boolean isMember() {
        return member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    @Override
    public String toString(){
        String selectedString = member ? "isMemeber" : "not Member";
        String isTreasurer = treasurer? "isTreasurer" : "Not Treasurere";

        return name+" -> "+selectedString+ " with value "+name + "is treasure "+isTreasurer;
    }

}
