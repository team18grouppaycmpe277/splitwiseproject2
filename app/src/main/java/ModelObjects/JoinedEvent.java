package ModelObjects;

/**
 * Created by Vaibhav on 4/26/2015.
 */
public class JoinedEvent {
    private String grpId;
    private String eventId;
    private String userId;
    private String eventName;

    public void setGrpId(String grpId){
        this.grpId = grpId;
    }
    public String getGrpId(){
        return grpId;
    }

    public void setEventId(String eventId){
        this.eventId = eventId;
    }
    public String getEventId(){
        return eventId;
    }

    public void setUserId(String userId){
        this.userId = userId;
    }
    public String getUserId(){
        return userId;
    }

    public void setEventName(String eventName){this.eventName = eventName;}
    public String getEventName(){return eventName;}
}
