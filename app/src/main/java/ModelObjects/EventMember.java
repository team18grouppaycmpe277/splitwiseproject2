package ModelObjects;

/**
 * Created by Vaibhav on 4/29/2015.
 */
public class EventMember {
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getUserBillAmount() {
        return userBillAmount;
    }

    public void setUserBillAmount(Double userBillAmount) {
        this.userBillAmount = userBillAmount;
    }

    private String userId;
    private String userName;
    private Double userBillAmount;
}
