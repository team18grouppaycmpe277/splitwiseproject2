package ModelObjects;

/**
 * Created by Vaibhav on 4/25/2015.
 */
public class GroupEvent {

    private String grpId;
    private String eventId;
    private String eventName;
    private String eventDescription;
    private String eventDate;
    private String eventType;
    private String eventOwner;

    public String getGrpId()
    {
        return grpId;
    }

    public void setGrpId(String grpId)
    {
        this.grpId = grpId;
    }

    public String getEventId()
    {
        return eventId;
    }

    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName)
    {
        this.eventName = eventName;
    }

    public String getEventDescription()
    {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription)
    {
        this.eventDescription = eventDescription;
    }
    public String getEventDate()
    {
        return eventDate;
    }

    public void setEventDate(String eventDate)
    {
        this.eventDate = eventDate;
    }
    public String getEventType()
    {
        return eventType;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }
    public String getEventOwner()
    {
        return eventOwner;
    }

    public void setEventOwner(String eventOwner)
    {
        this.eventOwner = eventOwner;
    }


    public boolean isJoined() {
        return isJoined;
    }

    public void setJoined(boolean isJoined) {
        this.isJoined = isJoined;
    }

    private boolean isJoined = true;

}
