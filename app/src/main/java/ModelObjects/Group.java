package ModelObjects;

import bolts.Bolts;

/**
 * Created by Niveditha on 4/21/2015.
 */
public class Group {

    public String getGrpId() {
        return grpId;
    }

    public void setGrpId(String grpId) {
        this.grpId = grpId;
    }

    private String grpId;
    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public boolean isJoined() {
        return isJoined;
    }

    public void setJoined(boolean isJoined) {
        this.isJoined = isJoined;
    }

    private String grpName;
    private boolean isJoined;
}
