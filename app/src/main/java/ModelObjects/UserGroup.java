package ModelObjects;

/**
 * Created by Vaibhav on 4/20/2015.
 */
public class UserGroup {

    private String grpName;
    private String grpId;


    public String getGrpId() {
        return grpId;
    }

    public void setGrpId(String grpId) {
        this.grpId = grpId;
    }


    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }


}
