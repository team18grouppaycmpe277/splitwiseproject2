package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.Friend;
import ModelObjects.GroupEvent;

public class Treasure_Event_Member_List_Adapter extends ArrayAdapter {

    private final Activity context;
    private final List<Friend> treasurer_member_of_event;

    public Treasure_Event_Member_List_Adapter(Activity context, List<Friend> treasurer_member_of_event) {
        super(context, R.layout.activity_treasure__event__member__list__adapter, treasurer_member_of_event);
        this.context = context;
        this.treasurer_member_of_event =treasurer_member_of_event;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.activity_treasure__event__member__list__adapter, null, true);
        TextView groupNameText = (TextView) rowView.findViewById(R.id.Treasure_Event_Members);
        groupNameText.setText(treasurer_member_of_event.get(position).getName());
        return rowView;
    }
}



