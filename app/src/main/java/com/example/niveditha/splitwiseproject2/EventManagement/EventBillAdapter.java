package com.example.niveditha.splitwiseproject2.EventManagement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.example.niveditha.splitwiseproject2.UserGroupsManagement.UserGroupActivity;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

import ModelObjects.EventBill;
import ModelObjects.GroupEvent;
import ModelObjects.JoinedEvent;


public class EventBillAdapter extends ArrayAdapter {
    private final Activity context;
    private List<EventBill> eventBills = null;

    EventBillAdapter(Activity context, List<EventBill> eventBills) {
        super(context, R.layout.fragment_event_bill_adapter, eventBills);
        this.context = context;
        this.eventBills = eventBills;
    }

    static class ViewHolder {
        protected TextView title;
        protected TextView subTitle;
        protected TextView amount;
        protected CheckBox isApproved;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.fragment_event_bill_adapter, null, true);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.event_bill_desc);
            viewHolder.subTitle = (TextView) view.findViewById(R.id.event_bill_date);
            viewHolder.amount = (TextView) view.findViewById(R.id.event_bill_amount);
            viewHolder.isApproved = (CheckBox) view.findViewById(R.id.isApprovedBill);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if (eventBills.get(position) != null) {
            final EventBill bill = eventBills.get(position);
            viewHolder.title.setText(bill.getBillDescription());
            viewHolder.subTitle.setText(bill.getBillDate());
            viewHolder.amount.setText("$ " + bill.getAmountSpent().toString());
            if (bill.getIsApproved().equals("true")) {
                viewHolder.title.setTextColor(Color.parseColor("#ff4e7046"));
                viewHolder.amount.setTextColor(Color.parseColor("#ff4e7046"));
                viewHolder.subTitle.setTextColor(Color.parseColor("#ff000000"));
                viewHolder.isApproved.setChecked(true);
            } else {
                viewHolder.title.setTextColor(Color.parseColor("#FFB20715"));
                viewHolder.amount.setTextColor(Color.parseColor("#FFB20715"));
                viewHolder.isApproved.setChecked(false);
            }
        }

        return view;
    }

}