package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Context;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.Friend;


public class CreateGroupActivity extends ActionBarActivity implements View.OnClickListener{

    ListView listView;
    Button save;
    EditText grpName;
    List<Friend> myfriendsList = new ArrayList<Friend>();
    FriendListAdapter adapter;
    private RadioButton mSelectedRB;
    String groupId = "";
    private int mSelectedPosition = -1;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_create_group);
        getCount();
        getFriends();
        listView = (ListView) findViewById(R.id.friend_list);
        grpName = (EditText) findViewById(R.id.groupName);
        final String username = (String) getIntent().getExtras().get("username");
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            int count = 0;

            @Override
            public void onClick(View view) {
                String ownerId = "";
                String treasureId = "";

                for(Friend friend:myfriendsList){
                    if(friend.isTreasurer())
                        treasureId = friend.getUserId();
                    if(friend.getName().equals(username))
                        ownerId = friend.getUserId();
                }
                final ParseObject group = new ParseObject("Group");
               // group.put("grpName",);
                group.put("ownerId", ownerId);
                group.put("treasureId", treasureId);
                group.put("grpName",grpName.getText().toString());
                group.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        groupId = group.getObjectId();
                        for(Friend friend:myfriendsList)
                        {
                            if(friend.isMember())
                            {
                                ParseObject userGrp = new ParseObject("User_Group");
                                userGrp.put("grpId",groupId);
                                userGrp.put("grpName",group.get("grpName"));
                                userGrp.put("userId",friend.getUserId());
                                userGrp.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        finish();
                                    }
                                });
                            }
                        }
                    }
                });

            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        return true;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    private int getCount()
    {
        ParseQuery query = ParseQuery.getQuery(ParseUser.class);
        try {
            return query.count();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }
    private void getFriends()
    {
        ParseQuery query = ParseUser.getQuery();
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> friendList, ParseException e) {
                if (e == null) {
                    for(ParseUser user: friendList)
                    {

                      Friend userr = new Friend();
                      userr.setName(user.getUsername());
                      userr.setUserId(user.getObjectId());
                      myfriendsList.add(userr);
                      Log.i("User Name ", (String) user.getUsername());

                    }
                    adapter = new FriendListAdapter(CreateGroupActivity.this, myfriendsList);
                    listView.setAdapter(adapter);
                    return ;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                    return ;
                }
            }
        });


    }
}
