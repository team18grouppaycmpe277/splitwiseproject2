package com.example.niveditha.splitwiseproject2.UserGroupsManagement;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.Calendar;
import java.text.DateFormat;

import android.widget.DatePicker;

import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.Date;

import ModelObjects.UserGroup;


public class CreateEventActivity extends ActionBarActivity {

    DateFormat formate = DateFormat.getDateInstance();
    Calendar calender = Calendar.getInstance();
    String userGroup = "";
    String userId = "";
    Button createEvent;
    EditText eventName;
    EditText eventDescription;
    EditText eventDate;
    Spinner eventType;
    String groupId = "";
    String event_Name = "";
    String event_Type ="";
    String event_Description ="";
    String event_Date ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        userGroup = (String) intent.getExtras().get("userGroup");
        groupId = (String) intent.getExtras().get("groupId");
        userId = (String) intent.getExtras().get("userId");
        setContentView(R.layout.activity_create_event);

        setTitle("Create New Event");

        createEvent = (Button) findViewById(R.id.create_event_id);
        eventName = (EditText) findViewById(R.id.editTextEventName);
        eventDescription = (EditText) findViewById(R.id.editTextDescription);
        eventDate = (EditText) findViewById(R.id.editDate);
        eventType = (Spinner) findViewById(R.id.event_type_spinner);
        updateDate();
        eventDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                setDate();
            }
        });

        createEvent.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            event_Date = eventDate.getText().toString();
            event_Description = eventDescription.getText().toString();
            event_Name = eventName.getText().toString();
            event_Type = eventType.getSelectedItem().toString();

                if(event_Name!=null&&!event_Name.equals("")&&
                        (event_Type.equalsIgnoreCase("Public")||event_Type.equalsIgnoreCase("Ad Hoc"))
                            &&event_Date!=null&&!event_Date.equals("")) {


                    setProgressBarIndeterminateVisibility(true);
                    final ParseObject newEvent = new ParseObject("Event");
                    newEvent.put("eventName", event_Name);
                    newEvent.put("eventDescription", event_Description);
                    newEvent.put("eventDate", event_Date);
                    newEvent.put("EventType",event_Type);
                    newEvent.put("grpId",groupId);
                    newEvent.put("eventOwner", userId);
                    newEvent.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            setProgressBarIndeterminateVisibility(false);
                        if(e==null) {
                            ((EditText) findViewById(R.id.editTextEventName)).setText("");
                            ((EditText) findViewById(R.id.editTextDescription)).setText("");
                            ((EditText) findViewById(R.id.editDate)).setText("");
                            String eventId = newEvent.getObjectId();
                            onConfirmJoinEvent(groupId , eventId, userId, event_Name, PreserveLogIn.getPrefUserFirstName(CreateEventActivity.this));
                            AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
                            builder.setMessage("New Event Created Successfully")
                                    .setTitle("Event Created!")
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            finish();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                            else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
                            builder.setMessage("Cannot Create an event")
                                    .setTitle("Internal Sever Error!")
                                    .setPositiveButton(android.R.string.ok, null);
                            AlertDialog dialog = builder.create();
                            ((EditText) findViewById(R.id.editTextEventName)).setText("");
                            ((EditText) findViewById(R.id.editTextDescription)).setText("");
                            ((EditText) findViewById(R.id.editDate)).setText("");
                            dialog.show();
                        }
                        }

                    });

                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
                    builder.setMessage("Event cannot create due to Missing Details")
                            .setTitle("Insufficient Details")
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });
    }



    public void updateDate(){
        eventDate.setText(formate.format(calender.getTime()));
    }

    public void setDate(){
        new DatePickerDialog(CreateEventActivity.this, date, calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
            calender.set(Calendar.YEAR, year);
            calender.set(Calendar.MONTH, monthOfYear);
            calender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDate();

        }
    };


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void onConfirmJoinEvent(String groupId , String eventId, String userId, String eventName, String firstName) {
        this.setProgressBarIndeterminateVisibility(true);
        final ParseObject joinEvent = new ParseObject("Join_Event");
        joinEvent.put("grpId", groupId);
        joinEvent.put("eventId", eventId);
        joinEvent.put("userId", userId);
        joinEvent.put("eventName", eventName);
        joinEvent.put("userFirstName",firstName);
        joinEvent.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                setProgressBarIndeterminateVisibility(false);
                if(e==null) {
                }
                else{
                }
            }
        });
    }
}
