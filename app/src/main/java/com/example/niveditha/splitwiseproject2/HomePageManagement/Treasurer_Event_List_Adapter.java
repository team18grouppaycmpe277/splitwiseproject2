package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.Group;
import ModelObjects.GroupEvent;

public class Treasurer_Event_List_Adapter extends ArrayAdapter {
    private final Activity context;
    private final List<GroupEvent> treasurer_group_events;

    public Treasurer_Event_List_Adapter(Activity context, List<GroupEvent> treasurer_group_events) {
        super(context, R.layout.treasurer_event_list, treasurer_group_events);
        this.context = context;
        this.treasurer_group_events = treasurer_group_events;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.treasurer_event_list, null, true);
        TextView groupNameText = (TextView) rowView.findViewById(R.id.TreasurerEventName);
        groupNameText.setText(treasurer_group_events.get(position).getEventName());
        return rowView;
    }
}







