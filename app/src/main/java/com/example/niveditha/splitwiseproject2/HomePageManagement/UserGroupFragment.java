package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.UserGroup;

/**
 * @author Vaibhav Namdev
 * User fragment to view group of logged in user
 *
 */
public class UserGroupFragment extends Fragment implements View.OnClickListener {

    String userName;
    String userId;
    UserGroupsAdapter adapter;
    ListView listView;
    View view;
    List<UserGroup> userGroup;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        Bundle data = getArguments();
        userName = data.getString("userName");
        userId = data.getString("userId");
        View view = inflater.inflate(R.layout.fragment_user_group, container, false);
        System.out.println("Here:");
        getGroups();
        listView =  (ListView) view.findViewById(R.id.friend_list);
        return view;
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.createGrp:
                Intent intent = new Intent(this.getActivity(),CreateGroupActivity.class );
                intent.putExtra("username", userName);
                startActivity(intent);
                break;
            case R.id.joinGrp:
                break;
            default:
                break;
        }
    }

    /**
     * Get groups of a user from database
     */

    public void getGroups() {
        ParseQuery<ParseObject> myGroup = ParseQuery.getQuery("User_Group");

        myGroup.whereEqualTo("userId", userId);

         userGroup = new ArrayList<>();
         myGroup.findInBackground(new FindCallback<ParseObject>() {
             public void done(List<ParseObject> my_groups, ParseException e) {
                 if (e == null) {
                     for (ParseObject g : my_groups) {
                         //retrived user to list of UserGroup
                         UserGroup group = new UserGroup();
                         group.setGrpId(g.get("grpId").toString());
                         group.setGrpName(g.get("grpName").toString());
                         userGroup.add(group);
                         System.out.println("Here:" + g.get("grpName").toString());
                     }
                     // listView.setAdapter(adapter);
                     adapter = new UserGroupsAdapter(UserGroupFragment.this.getActivity(), userGroup);
                     listView.setAdapter(adapter);
                     return;
                 } else {
                     Log.d("UserGroup", "Error: " + e.getMessage());
                 }
             }
         });
    }
}
