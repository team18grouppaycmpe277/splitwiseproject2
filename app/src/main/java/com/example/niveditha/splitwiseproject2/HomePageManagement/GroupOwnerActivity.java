package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.Group;


public class GroupOwnerActivity extends Fragment {

    ListView listView;
    List<Group> ownersGroup = new ArrayList<Group>();
    OwnerGrpsListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view =  inflater.inflate(R.layout.activity_group_owner, container, false);
        getGroupsOfOwner();
        listView = (ListView)view.findViewById(R.id.ownerGrps);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Group grpSelected = ownersGroup.get(position);
                Intent ownerIntent = new Intent(GroupOwnerActivity.this.getActivity(), OwnerFragment.class);
                ownerIntent.putExtra("groupId", grpSelected.getGrpId());
                ownerIntent.putExtra("groupName",grpSelected.getGrpName());
                startActivity(ownerIntent);
            }
        });
        return view;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getGroupsOfOwner(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Group");
        query.whereEqualTo("ownerId", ParseUser.getCurrentUser().getObjectId());

        Log.e("Nivedithaaaa " ,ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> grpList, ParseException e) {
                if (e == null) {
                    for (ParseObject g : grpList) {
                        Group group = new Group();
                        group.setGrpName((String)g.get("grpName"));
                        group.setGrpId(g.getObjectId());
                        ownersGroup.add(group);
                    }
                    adapter = new OwnerGrpsListAdapter(GroupOwnerActivity.this.getActivity(), ownersGroup);

                    listView.setAdapter(adapter);
                    return;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }
}

