package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v4.app.Fragment;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.Group;

public class Fragment_Treasurer_Group extends Fragment {
    ListView listView;
    List<Group> treasurersGroup = new ArrayList<Group>();
    TreasurerGrpsListAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_group_treasurer, container, false);
        getGroupsOfTreasurer();
        listView = (ListView) view.findViewById(R.id.treasurerGrps);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Group grpSelected = treasurersGroup.get(position);
                Intent treasurerIntent = new Intent(Fragment_Treasurer_Group.this.getActivity(), Treasurer_Activity.class);
                treasurerIntent.putExtra("groupId", grpSelected.getGrpId());
                treasurerIntent.putExtra("groupName", grpSelected.getGrpName());
                startActivity(treasurerIntent);
                Toast.makeText(Fragment_Treasurer_Group.this.getActivity().getApplicationContext(), "You Clicked at " + grpSelected.getGrpId(), Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Inflate the menu; this adds items to the action bar if it is present.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void getGroupsOfTreasurer() {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Group");
        query.whereEqualTo("treasureId", ParseUser.getCurrentUser().getObjectId());



                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> grpList, ParseException e) {
                            if (e == null) {
                    for (ParseObject g : grpList) {
                        Group group = new Group();
                        group.setGrpName((String) g.get("grpName"));
                        group.setGrpId(g.getObjectId());
                        treasurersGroup.add(group);
                    }

                    
                    adapter = new TreasurerGrpsListAdapter(Fragment_Treasurer_Group.this.getActivity(), treasurersGroup);

                    listView.setAdapter(adapter);
                    return;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }
}
