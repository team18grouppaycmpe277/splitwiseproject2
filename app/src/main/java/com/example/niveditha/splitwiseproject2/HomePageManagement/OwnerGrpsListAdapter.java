package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.Friend;
import ModelObjects.Group;

/**
 * Created by Niveditha on 4/22/2015.
 */
public class OwnerGrpsListAdapter extends ArrayAdapter {

    private final Activity context;
    private final List<Group> ownerGrps;

    public OwnerGrpsListAdapter(Activity context, List<Group> ownersGrpsList) {
        super(context, R.layout.owner_group_list, ownersGrpsList);
        this.context = context;
        this.ownerGrps = ownersGrpsList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.owner_group_list, null, true);
        TextView groupNameText = (TextView) rowView.findViewById(R.id.ownerGrpName);
        groupNameText.setText(ownerGrps.get(position).getGrpName());
        return rowView;
    }
}
