package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.Friend;
import ModelObjects.Group;

/**
 * Created by Niveditha on 4/21/2015.
 */
public class JoinGroupAdapter extends ArrayAdapter{


    private final Activity context;
    private final List<Group> otherGrps;
    private RadioButton mSelectedRB;
    private int mSelectedPosition = -1;
    JoinGroupAdapter(Activity context,List<Group> otherGrpsList){
        super(context, R.layout.other_groups,otherGrpsList);
        this.context = context;
        this.otherGrps = otherGrpsList;
    }
    static class ViewHolder {
        protected TextView text;
        protected CheckBox checkbox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.other_groups, null,true);
            viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.groupName1);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.isJoined);
            viewHolder.checkbox.setTag(position);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.text.setText(otherGrps.get(position).getGrpName());
        viewHolder.checkbox.setChecked(otherGrps.get(position).isJoined());
        viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox checkbox = (CheckBox) v;
                int getPosition = (Integer) checkbox.getTag();
                otherGrps.get(getPosition).setJoined(checkbox.isChecked());
            }
        });

        return convertView;
    }
}
