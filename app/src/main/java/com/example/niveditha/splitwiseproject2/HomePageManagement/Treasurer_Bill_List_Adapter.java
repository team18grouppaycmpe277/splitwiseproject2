package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.EventBill;
import ModelObjects.Friend;
import ModelObjects.GroupEvent;

/**
 * Created by SHRIRAM on 4/29/2015.
 */
public class Treasurer_Bill_List_Adapter  extends ArrayAdapter{
private final Activity context;
private final List<EventBill> bills_of_members;

    public Treasurer_Bill_List_Adapter(Activity context, List<EventBill> bills) {
        super(context, R.layout.treasurer_bill_list_adapter, bills);
        this.context = context;
        this.bills_of_members = bills;
    }
    static class ViewHolder {
        protected TextView billDescription;
        protected TextView billAmount;
        protected CheckBox checkbox;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.treasurer_bill_list_adapter, null, true);
            viewHolder = new ViewHolder();
            viewHolder.billDescription = (TextView) convertView.findViewById(R.id.TreasurerMemberBillDescription);
            viewHolder.billAmount = (TextView) convertView.findViewById(R.id.TreasurerMemberBillAmount);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.checkBox);
            viewHolder.checkbox.setTag(position);
            convertView.setTag(viewHolder);
        } else

        {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.billDescription.setText(bills_of_members.get(position).getBillDescription());
        viewHolder.billAmount.setText(bills_of_members.get(position).getAmountSpent().toString());


        viewHolder.checkbox.setChecked(bills_of_members.get(position).isBillApproved());
        viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox checkbox = (CheckBox) v;
                int getPosition = (Integer) checkbox.getTag();
                bills_of_members.get(getPosition).setBillApproved(checkbox.isChecked());
            }
        });





        return convertView;
    }





    }



