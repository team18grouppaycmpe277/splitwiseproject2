package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.Friend;


public class ApproveMemeberActivity extends ActionBarActivity {

    ListView membrView;
    List<Friend> membersList = new ArrayList<>();
    String groupId;
    String groupName;
    String frndId;
    ApproveMemberAdapter adapter;
    Button approveMember;
    @Override    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_memeber);
        groupId = getIntent().getExtras().getString("groupId");
        groupName = getIntent().getExtras().getString("groupName");

        membrView = (ListView)findViewById(R.id.otherMembers);
        getMembers();
        approveMember = (Button)findViewById(R.id.appMember);
        approveMember.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                    for(Friend friend:membersList)
                    {
                        if(friend.isMember())
                        {
                            ParseObject parseObject = new ParseObject("User_Group");
                            parseObject.put("grpName",groupName);
                            parseObject.put("grpId",groupId);
                            frndId = friend.getUserId();
                            parseObject.put("userId",friend.getUserId());
                            parseObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Join_Request");
                                    query.whereEqualTo("grpId", groupId);
                                    query.whereEqualTo("userId", frndId);
                                    query.findInBackground(new FindCallback<ParseObject>() {

                                        public void done(List<ParseObject> testList, ParseException e) {
                                            if (e == null) {
                                                Log.d("score", "Retrieved " + testList.size() + " test objects");
                                                for (ParseObject object:testList) {
                                                    object.deleteInBackground();
                                                }
                                            } else {
                                                Log.d("score", "Error: " + e.getMessage());
                                            }
                                        }
                                    });
                                    finish();
                                }
                            });
                        }
                       Log.i("This is apprvd members" ,friend.toString());
                    }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_approve_memeber, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getMembers()
    {

        ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Join_Request");
        innerQuery.whereEqualTo("grpId", groupId);

        innerQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> memList, ParseException e) {
                if (e == null) {
                    for (ParseObject g : memList) {
                        Friend fried = new Friend();
                        fried.setName((String) g.get("userName"));
                        fried.setUserId((String) g.get("userId"));
                        membersList.add(fried);
                    }
                    adapter = new ApproveMemberAdapter(ApproveMemeberActivity.this, membersList);
                    membrView.setAdapter(adapter);
                    return;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }
}
