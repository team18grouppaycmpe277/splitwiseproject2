package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Niveditha
 */
public class NotificationActivity extends ActionBarActivity{

    ListView notificationView;
    List<String> notification = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_notification);
        notificationView = (ListView)findViewById(R.id.user_notification);
        getNotification();

        return ;
    }

    private void getNotification()
    {
        ParseQuery<ParseObject> getPayPublicFund = ParseQuery.getQuery("User_Group");
        getPayPublicFund.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        getPayPublicFund.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                for (ParseObject parseObject : parseObjects) {
                    if(parseObject.get("requestedDonationAmnt") != null)
                     notification.add("Admin of group " + parseObject.get("grpName") + " has requested amount " + parseObject.get("requestedDonationAmnt"));
                }
               StableArrayAdapter notificationAdapter = new StableArrayAdapter(NotificationActivity.this,android.R.layout.simple_list_item_1, notification);
                notificationView.setAdapter(notificationAdapter);
            }
        }   );


    }
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View view = super.getView(position, convertView, parent);
            TextView text = (TextView) view.findViewById(android.R.id.text1);

            text.setTextColor(Color.BLUE);

            return view;
        };

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}
