package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by Niveditha on 4/22/2015.
 */

public class OwnerFragment extends ActionBarActivity implements View.OnClickListener {
    String youEditTextValue;
    String grpId;
    String grpName;
    AlertDialog.Builder viewMembers;
    ListView memberList;
    List<String> memberObjectIds;
    private ArrayAdapter<String> listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_owner);
        grpId = getIntent().getExtras().get("groupId").toString();
        grpName = getIntent().getExtras().get("groupName").toString();
        Button contribute = (Button) findViewById(R.id.contribute);
        Button approveMember = (Button) findViewById(R.id.appMember1);
      /*  viewMember.setOnClickListener(this);

        viewMember.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                viewMembers = new AlertDialog.Builder(OwnerFragment.this);
                memberList = new ListView(OwnerFragment.this.getApplicationContext());
                getMemebersOfGroup();
            }
        });
        */

        contribute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(OwnerFragment.this);

                final EditText edittext = new EditText(OwnerFragment.this.getApplicationContext());
                edittext.setTextColor(Color.parseColor("#00ff00"));
                alert.setMessage("Enter the amount");
                alert.setTitle("Contribution");

                alert.setView(edittext);

                alert.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //What ever you want to do with the value
                        youEditTextValue = edittext.getText().toString();
                        saveAmountRequested();
                    }
                });

                alert.show();
            }
        });

        approveMember.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.appMember1:
                Intent intent3 = new Intent(OwnerFragment.this, ApproveMemeberActivity.class);
                intent3.putExtra("groupId", grpId);
                intent3.putExtra("groupName", grpName);
                startActivity(intent3);
            default:
                break;
        }
    }

    private void saveAmountRequested() {

        ParseQuery<ParseObject> userIngrp = ParseQuery.getQuery("User_Group");
        userIngrp.whereEqualTo("grpId", grpId);

        userIngrp.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> usersOfGrp, ParseException e) {
                for (ParseObject users : usersOfGrp) {
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Group");

                    query.getInBackground(users.getObjectId(), new GetCallback<ParseObject>() {
                        public void done(ParseObject gameScore, ParseException e) {
                            if (e == null) {
                                gameScore.put("requestedDonationAmnt", youEditTextValue);
                                gameScore.saveInBackground();
                            }
                        }
                    });
                }

            }
        });
    }

    private void getMemebersOfGroup() {
        viewMembers = new AlertDialog.Builder(OwnerFragment.this);
        memberObjectIds = new ArrayList<>();
        ParseQuery<ParseObject> groupMembers = ParseQuery.getQuery("User_Group");
        groupMembers.whereEqualTo("grpId", grpId);


        groupMembers.findInBackground(new FindCallback<ParseObject>() {

            List<ParseObject> userList = new ArrayList<ParseObject>();

            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                for (ParseObject member : parseObjects) {
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
                    query.whereEqualTo("objectId", member.get("userId").toString());
                    Log.i("Nivedithaaaaa user id ", member.get("userId").toString());
                    try {
                        userList = query.find();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    for (ParseObject user : userList) {
                        memberObjectIds.add(user.get("username").toString());
                    }
                    viewMembers.setMessage("Memebers");
                    viewMembers.setTitle("Group Memebers");

                    viewMembers.setView(memberList);

                    viewMembers.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    memberList.setAdapter(listAdapter);
                    viewMembers.show();
                }
            }

        });
    }
}
