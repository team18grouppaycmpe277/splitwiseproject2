package com.example.niveditha.splitwiseproject2.UserGroupsManagement;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v7.app.ActionBar.Tab;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.HomePageManagement.GroupOwnerActivity;
import com.example.niveditha.splitwiseproject2.HomePageManagement.HomeScreenFragment;
import com.example.niveditha.splitwiseproject2.HomePageManagement.UserGroupFragment;
import com.example.niveditha.splitwiseproject2.R;

import java.util.ArrayList;
import java.util.List;

public class UserGroupActivity extends ActionBarActivity implements android.support.v7.app.ActionBar.TabListener {

    private String[] tabs = { "All Events" , "My Events"};
    List<Fragment> fragList = new ArrayList<Fragment>();
    private String userGroup= "";
    private String userId = "";
    private String groupId = "";
    private TextView userTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        userGroup = (String) intent.getExtras().get("userGroup");
        userId = (String) intent.getExtras().get("userId");
        groupId = (String) intent.getExtras().get("groupId");
        setContentView(R.layout.activity_user_group);
        setTitle(userGroup);

        ActionBar.Tab tab1, tab2;

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);

        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.add_Event:
                Intent intent = new Intent(UserGroupActivity.this, CreateEventActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("userId", userId);
                intent.putExtra("userGroup", userGroup);
                intent.putExtra("groupId",groupId);
                startActivity(intent);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {

        Fragment f = null;
        Fragment tf = null;
        if (fragList.size() > tab.getPosition())
            fragList.get(tab.getPosition());


        if (f == null) {
            Bundle data = new Bundle();
            data.putString("userGroup",userGroup);
            data.putString("userId", userId);
            data.putString("groupId",groupId);
            
            if(tab.getPosition() == 0) {
                tf = new GroupEventFragment();
                tf.setArguments(data);
            }
            else if(tab.getPosition() ==1) {
                tf = new JoinedEventFragement();
                tf.setArguments(data);
            }

            else
                tf = new GroupEventFragment();

            fragList.add(tf);
        }
        else
            tf = (GroupEventFragment) f;

        fragmentTransaction.replace(android.R.id.content, tf);

    }


    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {

    }



}
