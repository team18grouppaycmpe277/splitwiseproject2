package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.Friend;

public class Treasure_Event_Members extends ActionBarActivity {


    ListView listview;
    List<Friend> event_members = new ArrayList<Friend>();
    Treasure_Event_Member_List_Adapter members_of_event_list_adapter;
    String currentEventId;
    String currentEventName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.treasure_event_members);


        currentEventId = getIntent().getExtras().get("evetId").toString();
        currentEventName = getIntent().getExtras().get("eventName").toString();
        getMembersOfEvent();

        listview = (ListView) findViewById(R.id.treasurerlistofmembers);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
            int position, long id) {

                Friend members_of_event = event_members.get(position);

                Intent group_event_members_intent = new Intent(Treasure_Event_Members.this, Treasurer_Member_Bills.class);
                group_event_members_intent.putExtra("userId", members_of_event.getUserId());
                group_event_members_intent.putExtra("eventId", currentEventId);
                group_event_members_intent.putExtra("eventName",currentEventName);
                startActivity(group_event_members_intent);
            }


        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_treasure__event__members, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void getMembersOfEvent(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Join_Event");
        query.whereEqualTo("eventId",currentEventId );

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventlist, ParseException e) {
                if (e == null) {
                    for (ParseObject g : eventlist) {
                        Friend members_of_event = new Friend();

                        members_of_event.setUserId(g.get("userId").toString());
                        members_of_event.setName(g.get("userFirstName").toString());
                        event_members.add(members_of_event);
                    }
                    //Friend ge  = new Friend();
                    //ge.setUserId("qwe123weq2");
                    //ge.setName("My Name");
                    //event_members.add(ge);
                    members_of_event_list_adapter = new Treasure_Event_Member_List_Adapter(Treasure_Event_Members.this, event_members);
                    listview.setAdapter(members_of_event_list_adapter);
                    return;


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }

            }
        });


    }


}




