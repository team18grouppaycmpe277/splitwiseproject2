package com.example.niveditha.splitwiseproject2.UserGroupsManagement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.GroupEvent;
import ModelObjects.JoinedEvent;

/**
 * Created by Vaibhav on 4/25/2015.
 */
public class EventAdapter  extends ArrayAdapter {
    private final Activity context;
    private final List<GroupEvent> groupEvents;
    private final String userGroup;
    private List<JoinedEvent> joinEvent = null;
    EventAdapter(Activity context, List<GroupEvent> groupEvents, String userGroup) {
        super(context, R.layout.fragment_events_adapter, groupEvents);
        this.context = context;
        this.groupEvents = groupEvents;
        this.userGroup = userGroup;
    }

    static class ViewHolder {
        protected TextView title;
        protected TextView subTitle;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.fragment_events_adapter, null,true);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.event_list_name);
            viewHolder.subTitle = (TextView) view.findViewById(R.id.event_list_subName);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if(groupEvents.get(position) != null )
        {
            final GroupEvent event = groupEvents.get(position);
            viewHolder.title.setText(groupEvents.get(position).getEventName());
            viewHolder.subTitle.setText(groupEvents.get(position).getEventDate());
            viewHolder.title.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){

                    ParseQuery<ParseObject> joinedEvents = ParseQuery.getQuery("Join_Event");
                    joinedEvents.whereEqualTo("userId", PreserveLogIn.getPrefUserId(context));
                    joinedEvents.whereEqualTo("grpId", groupEvents.get(position).getGrpId());
                    joinedEvents.whereEqualTo("eventId", groupEvents.get(position).getEventId());
                    joinedEvents.findInBackground(new FindCallback<ParseObject>(){
                        public void done(List<ParseObject> retrivedEvents, ParseException e) {
                            if (e == null) {
                                if (retrivedEvents.isEmpty()) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Event Details:\n " + groupEvents.get(position).getEventDescription() + "\n\n" +
                                            "Event Type: " + groupEvents.get(position).getEventType())
                                            .setTitle("Join Event: " + groupEvents.get(position).getEventName())
                                            .setPositiveButton("Join Event", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Logic to Join an Event
                                                    onConfirmJoinEvent(groupEvents.get(position).getGrpId(), groupEvents.get(position).getEventId(),
                                                            PreserveLogIn.getPrefUserId(context), groupEvents.get(position).getEventName(),
                                                                PreserveLogIn.getPrefUserFirstName(context));
                                                    Intent intent = new Intent(context, UserGroupActivity.class);
                                                    intent.putExtra("userGroup", userGroup);
                                                    intent.putExtra("userId", PreserveLogIn.getPrefUserId(context));
                                                    intent.putExtra("groupId", groupEvents.get(position).getGrpId());
                                                    context.startActivity(intent);
                                                }
                                            });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Cancle is to just Cancel and return to page
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                  }
                                else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Event Details:\n " + groupEvents.get(position).getEventDescription() + "\n\n" +
                                            "Event Type: " + groupEvents.get(position).getEventType())
                                            .setTitle("Already Joined Event: " + groupEvents.get(position).getEventName())
                                            .setNegativeButton("GO BACK", null);
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }
                        }
                    });


                }
            });

        }

        return view;
    }


    private void onConfirmJoinEvent(String groupId , String eventId, String userId, String eventName, String firstName) {
        context.setProgressBarIndeterminateVisibility(true);
        final ParseObject joinEvent = new ParseObject("Join_Event");
        joinEvent.put("grpId", groupId);
        joinEvent.put("eventId", eventId);
        joinEvent.put("userId", userId);
        joinEvent.put("eventName", eventName);
        joinEvent.put("userFirstName",firstName);
        joinEvent.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null) {
                    CharSequence text = "SuccessFully Joined Event!";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
                else{
                    CharSequence text = "Failed to Join Event!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
        context.setProgressBarIndeterminateVisibility(false);
    }
}
