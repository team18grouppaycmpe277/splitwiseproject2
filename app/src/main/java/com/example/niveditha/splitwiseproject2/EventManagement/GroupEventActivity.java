package com.example.niveditha.splitwiseproject2.EventManagement;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.niveditha.splitwiseproject2.R;
import com.example.niveditha.splitwiseproject2.UserGroupsManagement.CreateEventActivity;
import com.example.niveditha.splitwiseproject2.UserGroupsManagement.GroupEventFragment;
import com.example.niveditha.splitwiseproject2.UserGroupsManagement.JoinedEventFragement;

import java.util.ArrayList;
import java.util.List;


public class GroupEventActivity extends ActionBarActivity implements android.support.v7.app.ActionBar.TabListener {

    private String[] tabs = { "Bills", "View Report"};
    List<Fragment> fragList = new ArrayList<Fragment>();
    String groupId = "";
    String userId = "";
    String eventId = "";
    String eventName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        groupId = (String) intent.getExtras().get("groupId");
        userId = (String) intent.getExtras().get("userId");
        eventId = (String) intent.getExtras().get("eventId");
        eventName = (String) intent.getExtras().get("eventName");
        setTitle(eventName);
        setContentView(R.layout.activity_group_event);
        android.app.ActionBar.Tab tab1, tab2;
        Fragment fragmentTab1 = new Fragment();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);

        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_Event:
                Intent intent = new Intent(GroupEventActivity.this, AddBillActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("eventId", eventId);
                intent.putExtra("groupId", groupId);
                startActivity(intent);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        Fragment f = null;
        Fragment tf = null;

        if (fragList.size() > tab.getPosition())
            fragList.get(tab.getPosition());


        if (f == null) {
            Bundle data = new Bundle();
            data.putString("eventId", eventId);
            data.putString("groupId",groupId);

            if(tab.getPosition() == 0) {
                tf = new EventBillsFragment();
                tf.setArguments(data);
            }
            else if(tab.getPosition() ==1) {
                tf = new ViewReport();
                tf.setArguments(data);
            }

            else
                tf = new EventBillsFragment();

            fragList.add(tf);
        }
        else
            tf = (EventBillsFragment) f;

        fragmentTransaction.replace(android.R.id.content, tf);


    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
