package com.example.niveditha.splitwiseproject2.HomePageManagement;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.example.niveditha.splitwiseproject2.UserGroupsManagement.UserGroupActivity;

import java.util.List;
import ModelObjects.UserGroup;

/**
 * Created by Vaibhav on 4/22/2015.
 */
public class UserGroupsAdapter extends ArrayAdapter{
    private final Activity context;
    private final List<UserGroup> userGroups;

    UserGroupsAdapter(Activity context, List<UserGroup> userGroups) {
        super(context, R.layout.group_list, userGroups);
        this.context = context;
        this.userGroups = userGroups;
    }
    static class ViewHolder {
        protected TextView text;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.group_list, null,true);
            viewHolder = new ViewHolder();
            viewHolder.text = (TextView) view.findViewById(R.id.listView);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if(userGroups.get(position) != null )
        {
            final UserGroup user = userGroups.get(position);
            viewHolder.text.setText(userGroups.get(position).getGrpName());
            viewHolder.text.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String userId = PreserveLogIn.getPrefUserId(context).toString();
                Intent intent = new Intent(context, UserGroupActivity.class);
                intent.putExtra("userId", userId);
                intent.putExtra("userGroup", user.getGrpName());
                intent.putExtra("groupId",user.getGrpId());
                context.startActivity(intent);
                }
        });

        }

        return view;
    }
}