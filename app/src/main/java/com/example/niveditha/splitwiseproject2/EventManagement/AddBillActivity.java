package com.example.niveditha.splitwiseproject2.EventManagement;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import java.util.Calendar;
import java.text.DateFormat;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;


import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class AddBillActivity extends ActionBarActivity {
    String groupId = "";
    String eventId = "";
    String userId ="";
    String billDesc="";
    String amount="";
    String dateBill = "";
    Button addBill;
    Button billDate;
    Button cancelBtn;
    EditText billDescription;
    EditText billAmount;
    String isApproved = "";
    DateFormat formate = DateFormat.getDateInstance();
    Calendar calender = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bill);
        Intent intent = getIntent();
        eventId = (String) intent.getExtras().get("eventId");
        groupId = (String) intent.getExtras().get("groupId");
        userId = PreserveLogIn.getPrefUserId(this);
        setContentView(R.layout.activity_add_bill);
        setTitle("Add a Bill");

        billDescription = (EditText) findViewById(R.id.editTextEventName);
        billAmount = (EditText) findViewById(R.id.editTextBillAmount);
        billDate = (Button) findViewById(R.id.editTextBillDate);
        addBill = (Button) findViewById(R.id.button_add_bill);
        cancelBtn = (Button) findViewById(R.id.button_cancel_bill);
        updateDate();
        billDate.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    setDate();
                }
        });

        addBill.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {

                    billDesc = billDescription.getText().toString();
                    amount = billAmount.getText().toString();
                    dateBill = billDate.getText().toString();

                    isApproved = "false";

                    if(!billDesc.equals("")&& billDesc !=null &&
                            !amount.equals("") && amount!=null &&
                            date!=null && !date.equals("")){

                        setProgressBarIndeterminateVisibility(true);
                        final ParseObject putBill = new ParseObject("User_Event");
                        putBill.put("eventId", eventId);
                        putBill.put("userId", userId);
                        putBill.put("billDescription", billDesc);
                        putBill.put("amountSpent", Double.parseDouble(amount));
                        putBill.put("billDate",dateBill);
                        putBill.put("isApproved", isApproved);
                        putBill.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                setProgressBarIndeterminateVisibility(false);
                                if(e==null) {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(AddBillActivity.this);
                                    builder.setMessage("Bill added Successfully")
                                            .setTitle("Bill Added!")
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    finish();
                                                }
                                            });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                                else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AddBillActivity.this);
                                    builder.setMessage("Error! adding a Bill")
                                            .setTitle("No Response")
                                            .setPositiveButton("Go Back", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    finish();
                                                }
                                            });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }
                        });

                    }

                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddBillActivity.this);
                        builder.setMessage("Bill cannot added due to missing Details")
                                .setTitle("Insufficient Details")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void updateDate(){
        billDate.setText(formate.format(calender.getTime()));
    }


    public void setDate(){
        new DatePickerDialog(AddBillActivity.this, date, calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener(){
        @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
                    calender.set(Calendar.YEAR, year);
                    calender.set(Calendar.MONTH, monthOfYear);
                    calender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateDate();

        }
    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_bill, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createBill(){
        billDesc = billDescription.getText().toString();
        amount = billAmount.getText().toString();
        dateBill = billDate.getText().toString();
            if(!billDesc.equals("")&& billDesc !=null &&
                    !amount.equals("") && amount!=null &&
                            date!=null && !date.equals("")){

                setProgressBarIndeterminateVisibility(true);
                final ParseObject putBill = new ParseObject("User_Event");
                putBill.put("eventId", eventId);
                putBill.put("userId", userId);
                putBill.put("billDescription", billDesc);
                putBill.put("amountSpent", amount);
                putBill.put("billDate",dateBill);
                putBill.put("isApproved", false);
                putBill.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(AddBillActivity.this);
                            builder.setMessage("Bill added Successfully")
                                    .setTitle("Bill Added!")
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            finish();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddBillActivity.this);
                            builder.setMessage("Error! adding a Bill")
                                    .setTitle("No Response")
                                    .setPositiveButton("Go Back", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            finish();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                });

            }

            else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddBillActivity.this);
                    builder.setMessage("Bill cannot added due to missing Details")
                            .setTitle("Insufficient Details")
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
    }

}
