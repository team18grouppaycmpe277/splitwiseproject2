package com.example.niveditha.splitwiseproject2.EventManagement;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.example.niveditha.splitwiseproject2.UserGroupsManagement.EventAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.EventBill;
import ModelObjects.GroupEvent;
import ModelObjects.JoinedEvent;

public class EventBillsFragment extends Fragment implements View.OnClickListener {

    String groupId;
    String userId;
    EventBillAdapter adapter;
    ListView listView;
    View view;
    String eventId="";
    List<EventBill> eventBill;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle data = getArguments();
        eventId = data.getString("eventId");
        userId = PreserveLogIn.getPrefUserId(this.getActivity());
        groupId = data.getString("groupId");
        View view = inflater.inflate(R.layout.fragment_event_bills, container, false);
        getBills();
        listView = (ListView) view.findViewById(R.id.event_bills_list);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createGrp:
                break;
            case R.id.joinGrp:
                break;
            default:
                break;
        }
    }

    public void getBills() {
        EventBillsFragment.this.getActivity().setProgressBarIndeterminateVisibility(true);
        ParseQuery<ParseObject> eventBills = ParseQuery.getQuery("User_Event");
        eventBills.whereEqualTo("eventId", eventId);
        eventBill = new ArrayList<>();
        eventBills.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> retrivedBills, ParseException e) {
                if (e == null) {
                    if(!retrivedBills.isEmpty()) {
                        for (ParseObject g : retrivedBills) {
                            //retrived user to list of UserGroup
                            EventBill bill = new EventBill();
                            bill.setBillId(g.getObjectId());
                            bill.setEventId(g.get("eventId").toString());
                            bill.setBillDescription(g.get("billDescription").toString());
                            bill.setUserId(g.get("userId").toString());
                            bill.setAmountSpent(Double.parseDouble(g.get("amountSpent").toString()));
                            bill.setBillDate(g.get("billDate").toString());
                            bill.setIsApproved(g.get("isApproved").toString());
                            eventBill.add(bill);
                        }
                    }
                    else{
                        CharSequence text = "No Bills yet!\nYou can add bill\nClick on add icon to add a bill";
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(EventBillsFragment.this.getActivity(), text, duration);
                        toast.show();
                    }

                    listView.setAdapter(adapter);
                    adapter = new EventBillAdapter(EventBillsFragment.this.getActivity(), eventBill);
                    listView.setAdapter(adapter);

                } else {
                    Log.d("UserGroup", "Error: " + e.getMessage());
                }
            }
        });
    }
}




