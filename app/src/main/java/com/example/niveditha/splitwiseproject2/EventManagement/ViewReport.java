package com.example.niveditha.splitwiseproject2.EventManagement;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ModelObjects.EventBill;
import ModelObjects.EventMember;


public class ViewReport extends Fragment implements View.OnClickListener {

    String groupId;
    Double total=0.00;
    Double owe = 0.00;
    Double owed = 0.00;
    Double yourAmount=0.00;
    TextView textViewTotalBalance;
    TextView textViewOwe;
    TextView textViewOwed;
    String userId;
    ViewReportAdapter adapter;
    ViewReportAdapter adapter2;
    ListView listViewYouOwe;
    ListView listViewYouOwed;
    View view;
    String eventId="";
    DecimalFormat df = new DecimalFormat("#.00");
    List<EventMember> eventMembers;
    List<EventMember> oweMember = new ArrayList<>();
    List<EventMember> owedMember = new ArrayList<>();
    HashMap<EventMember,Double> sumUpAmount = new HashMap<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle data = getArguments();
        eventId = data.getString("eventId");
        userId = PreserveLogIn.getPrefUserId(this.getActivity());
        groupId = data.getString("groupId");
        View view = inflater.inflate(R.layout.fragment_view_report, container, false);
        generateReport();
        textViewTotalBalance = (TextView) view.findViewById(R.id.textViewTotalBalance);
        textViewOwe = (TextView) view.findViewById(R.id.textViewOwe);
        textViewOwed = (TextView) view.findViewById(R.id.textViewOwnedTotal);
        listViewYouOwe = (ListView) view.findViewById(R.id.listViewYouOwe);
        listViewYouOwed =  (ListView) view.findViewById(R.id.listViewYouOwed);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createGrp:
                break;
            case R.id.joinGrp:
                break;
            default:
                break;
        }
    }



    public void generateReport() {
        ViewReport.this.getActivity().setProgressBarIndeterminateVisibility(true);
        final ParseQuery<ParseObject> eventMember = ParseQuery.getQuery("Join_Event");
        eventMember.whereEqualTo("eventId", eventId);
        eventMembers = new ArrayList<>();
        eventMember.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> retrivedMembers, ParseException e) {
                if (e == null) {
                    if(!retrivedMembers.isEmpty()) {
                        for (ParseObject g : retrivedMembers) {
                            //retrived user to list of UserGroup
                            EventMember member = new EventMember();
                            member.setUserId(g.get("userId").toString());
                            member.setUserBillAmount(Double.parseDouble("0.00"));
                            member.setUserName(g.get("userFirstName").toString());

                            //Retriving Bills of Members
                            List<ParseObject> userBills = null;
                            ParseQuery<ParseObject> isBills = ParseQuery.getQuery("User_Event");
                            isBills.whereEqualTo("eventId", eventId);
                            isBills.whereEqualTo("userId",member.getUserId());
                            isBills.whereEqualTo("isApproved","true");
                            try {
                                userBills = isBills.find();
                            } catch (ParseException e1) {
                                CharSequence text = "Error!\n";
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(ViewReport.this.getActivity(), text, duration);
                                toast.show();
                            }
                            for(ParseObject u: userBills){
                                Double totalAmount = member.getUserBillAmount() + Double.parseDouble(u.get("amountSpent").toString()) ;
                                member.setUserBillAmount(totalAmount);
                            }
                            eventMembers.add(member);
                        }
                    }

                    else{
                        CharSequence text = "Not any report!\n";
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(ViewReport.this.getActivity(), text, duration);
                        toast.show();
                    }


                    int totalMembers = eventMembers.size();
                    int myPosition;
                    for(EventMember m: eventMembers){
                        int i=0;
                        if(m.getUserId().equals(userId)){
                            myPosition = i;
                            Double amount = m.getUserBillAmount();
                            yourAmount = amount/totalMembers;
                            i++;
                        }
                        else{
                            Double amount = m.getUserBillAmount();
                            Double tempAmount = amount/totalMembers;
                            amount = tempAmount;
                            sumUpAmount.put(m,amount);
                            i++;
                        }
                    }

                    for(EventMember m: eventMembers){
                        if(!m.getUserId().equals(userId)){
                            Double finalAmount = sumUpAmount.get(m);
                            finalAmount = yourAmount - finalAmount;
                            sumUpAmount.put(m,finalAmount);
                            if(finalAmount>0){
                                owedMember.add(m);
                                owed = finalAmount+owed;
                            }
                            else if(finalAmount<0){
                                oweMember.add(m);
                                owe = (finalAmount*(-1))+owe;
                            }
                        }
                    }

                    if(owed>owe){
                        total = owed-owe;
                        textViewTotalBalance.setText("$" + df.format(total).toString());

                    }
                    else if(owed<owe){
                        total = owe - owed;
                        textViewTotalBalance.setTextColor(Color.parseColor("#ffc0000f"));
                        textViewTotalBalance.setText("$" + df.format(total).toString());
                    }
                    else{
                        textViewTotalBalance.setText("$0.00");
                    }

                    if(owe != 0.00)
                    {
                        textViewOwe.setText("$"+df.format(owe).toString());
                    }
                    if(owed != 0.00)
                    {
                       textViewOwed.setText("$"+df.format(owed).toString());

                    }




                    listViewYouOwe.setAdapter(adapter);
                    listViewYouOwed.setAdapter(adapter2);

                    adapter = new ViewReportAdapter(ViewReport.this.getActivity(), oweMember, sumUpAmount, "owe");
                    listViewYouOwe.setAdapter(adapter);

                    adapter2 = new ViewReportAdapter(ViewReport.this.getActivity(), owedMember, sumUpAmount, "owed");
                    listViewYouOwed.setAdapter(adapter2);
                } else {
                    Log.d("UserGroup", "Error: " + e.getMessage());
                }
            }
        });
    }

}
