package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.Group;

public class TreasurerGrpsListAdapter extends ArrayAdapter {
    private final Activity context;
    private final List<Group> treasurerGrps;

    public TreasurerGrpsListAdapter(Activity context, List<Group> treasurerGrpsList) {
        super(context, R.layout.treasurer_group_list, treasurerGrpsList);
        this.context = context;
        this.treasurerGrps = treasurerGrpsList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.treasurer_group_list, null, true);
        TextView groupNameText = (TextView) rowView.findViewById(R.id.TreasurerGrpName);
        groupNameText.setText(treasurerGrps.get(position).getGrpName());
        return rowView;
    }
}







