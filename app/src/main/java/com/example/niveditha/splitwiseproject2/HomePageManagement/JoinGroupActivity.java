package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ModelObjects.Group;


public class JoinGroupActivity extends ActionBarActivity {

    ListView listView;
    Button join;
    String username,userId;
    List<Group> grpList = new ArrayList<Group>();
    JoinGroupAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_join_group);
        getOtherGroups();
        listView = (ListView) findViewById(R.id.otherGrps);
        username = (String) getIntent().getExtras().get("username");
        join = (Button) findViewById(R.id.joinGrp);
        join.setOnClickListener(new View.OnClickListener() {
            int count = 0;

            @Override
            public void onClick(View view) {
                String ownerId = "";
                String treasureId = "";

                for(Group group :grpList){
                    if(group.isJoined())
                    {
                        ParseObject joinRequest = new ParseObject("Join_Request");
                        joinRequest.put("grpId",group.getGrpId());
                        joinRequest.put("userId",ParseUser.getCurrentUser().getObjectId());
                        joinRequest.put("grpName",group.getGrpName());
                        joinRequest.put("userName",ParseUser.getCurrentUser().getUsername());
                        joinRequest.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                            }
                        });
                    }
                }
                finish();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_join_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getOtherGroups()
    {

        final Set<String> mygroups = new HashSet<String>();
        ParseQuery<ParseObject> userIngrp = ParseQuery.getQuery("User_Group");
        userIngrp.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());

        userIngrp.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> mygrps, ParseException e) {

                if (e == null) {
                    for (ParseObject g : mygrps) {
                        mygroups.add((String)g.get("grpName"));
                    }
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Group");
                    query.whereNotEqualTo("userId", ParseUser.getCurrentUser().getObjectId());

                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> groupList, ParseException e) {


                            if (e == null) {
                                for (ParseObject g : groupList) {
                                    if (!mygroups.contains(g.get("grpName"))) {
                                        mygroups.add((String) g.get("grpName"));
                                        Group group = new Group();
                                        group.setGrpName((String) g.get("grpName"));
                                        group.setGrpId((String)g.get("grpId"));
                                        grpList.add(group);
                                    }
                                }
                                adapter = new JoinGroupAdapter(JoinGroupActivity.this, grpList);
                                listView.setAdapter(adapter);
                                return;
                            } else {
                                Log.d("score", "Error: " + e.getMessage());
                            }
                        }
                    });
                    return;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }
}
