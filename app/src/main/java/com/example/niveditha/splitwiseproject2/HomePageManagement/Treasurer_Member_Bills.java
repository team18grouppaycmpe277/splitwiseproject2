package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.EventBill;


public class Treasurer_Member_Bills extends ActionBarActivity {
    String currentEventId;
    String currentUserId;
    String currentEventName;
    ListView listview;
    List<EventBill> bills_of_members = new ArrayList<EventBill>();
    Treasurer_Bill_List_Adapter billAdapter;
    EventBill yourBill;
    Button Accept;
    Button Deny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treasure_member_bills);

        currentEventId = getIntent().getExtras().get("eventId").toString();
        currentUserId = getIntent().getExtras().get("userId").toString();
        currentEventName = getIntent().getExtras().get("eventName").toString();
        getdatafromdb();
        listview = (ListView) findViewById(R.id.listView3);


        Accept = (Button) findViewById(R.id.Accept);
        Accept.setOnClickListener (new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //String eventId = " ";
                //String userId  = " ";
                String answer;
                for (final EventBill your_Bill : bills_of_members) {
                    if (your_Bill.isBillApproved()) {
                        ParseQuery updateQuery = ParseQuery.getQuery("User_Event");
                        updateQuery.whereEqualTo("userId", currentUserId);
                        updateQuery.whereEqualTo("eventId", currentEventId);
                        updateQuery.findInBackground(new FindCallback<ParseObject>() {

                            @Override
                            public void done(List<ParseObject> parseObjects, ParseException e) {
                                for (ParseObject billEntry : parseObjects) {
                                    ParseQuery<ParseObject> updating = ParseQuery.getQuery("User_Event");
                                    if(billEntry.getObjectId().equals(your_Bill.getUniqueObjectId())){
                                    updating.getInBackground(billEntry.getObjectId(), new GetCallback<ParseObject>() {
                                        @Override
                                        public void done(ParseObject updateObject, ParseException e) {
                                            if (e == null) {
                                                updateObject.put("isApproved", "true");
                                                updateObject.saveInBackground(new SaveCallback() {
                                                    @Override
                                                    public void done(ParseException e) {

                                                    }
                                                });

                                            }
                                        }
                                    });}
                                }
                            }
                        });

                    }


                }
                finish();

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_treasure_member_bills, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getdatafromdb() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Event");
        query.whereEqualTo("eventId", currentEventId);
        query.whereEqualTo("userId", currentUserId);
        query.whereEqualTo("isApproved","false");
        query.findInBackground(new FindCallback<ParseObject>() {

            public void done(List<ParseObject> eventlist, ParseException e) {
                if (e == null) {


                    for (ParseObject g : eventlist) {
                        EventBill bills = new EventBill();
                        bills.setBillDescription((String) g.get("billDescription"));
                        bills.setAmountSpent(g.getDouble("amountSpent"));
                        bills.setUniqueObjectId(g.getObjectId());
                      //  bills.setIsBillApproved(false);
                        bills_of_members.add(bills);



                    }
                    billAdapter = new Treasurer_Bill_List_Adapter(Treasurer_Member_Bills.this,bills_of_members);
                    listview.setAdapter(billAdapter);
                    return;


                } else {
                    Log.d("score", "Error: " + e.getMessage());


                }
            }
        });
    }
}


