package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ModelObjects.Group;

/**
 * @author Niveditha
 */
public class NotificationFragment extends Fragment {

    ListView notificationView;
    List<String> notification = new ArrayList<String>();

    public NotificationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_notification, container, false);
        notificationView = (ListView)view.findViewById(R.id.user_notification);
        getNotification();

        return view;
    }

    private void getNotification()
    {
        ParseQuery<ParseObject> getPayPublicFund = ParseQuery.getQuery("User_Group");
        getPayPublicFund.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        getPayPublicFund.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                for (ParseObject parseObject : parseObjects) {
                    if(parseObject.get("requestedDonationAmnt") != null)
                     notification.add("Admin of group " + parseObject.get("grpName") + " has requested amount " + parseObject.get("requestedDonationAmnt"));
                }
               StableArrayAdapter notificationAdapter = new StableArrayAdapter(NotificationFragment.this.getActivity(),android.R.layout.simple_list_item_1, notification);
                notificationView.setAdapter(notificationAdapter);
            }
        }   );


    }
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View view = super.getView(position, convertView, parent);
            TextView text = (TextView) view.findViewById(android.R.id.text1);

            text.setTextColor(Color.BLUE);

            return view;
        };

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}
