package com.example.niveditha.splitwiseproject2.LogInManagement;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Vaibhav on 4/25/2015.
 */
public class PreserveLogIn {

    static final String PREF_USER_FIRST_NAME = "firstName";
    static final String PREF_USER_NAME= "username";
    static final String PREF_USER_ID= "userId";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setPrefUserId(Context ctx, String userId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_ID, userId);
        editor.commit();
    }

    public static String getPrefUserId(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_ID, "");
    }

    public static void setPrefUserName(Context ctx, String userName){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getPrefUserName(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void setPrefUserFirstName(Context ctx, String firstName){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_FIRST_NAME, firstName);
        editor.commit();
    }

    public static String getPrefUserFirstName(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_USER_FIRST_NAME, "");
    }


}


