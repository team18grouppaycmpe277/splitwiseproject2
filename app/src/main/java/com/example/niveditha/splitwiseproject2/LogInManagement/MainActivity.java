package com.example.niveditha.splitwiseproject2.LogInManagement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.HomePageManagement.HomeScreenActivity;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;

import com.parse.ParseUser;


public class MainActivity extends ActionBarActivity {

    protected EditText usernameEditText;
    protected EditText passwordEditText;
    protected Button loginButton;
    protected Button signupButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this, "v8XKnJp49ugCDT27VJ4poH6XcwJFxLjLjuvKrPB4", "65Ya1eMpSvU5rIcDpRt8D19cLeAJT5ScaeE5rrc8");
        ParseUser currentUser = ParseUser.getCurrentUser();

        if(PreserveLogIn.getPrefUserId(MainActivity.this).length() == 0
                || PreserveLogIn.getPrefUserName(MainActivity.this).length() == 0
                    || PreserveLogIn.getPrefUserFirstName(MainActivity.this).length() == 0)
        {
            setContentView(R.layout.activity_main);
            loadLoginView();
        }
        else
        {
            autoLogin(PreserveLogIn.getPrefUserName(MainActivity.this), PreserveLogIn.getPrefUserId(MainActivity.this));
        }



    }

    private void loadLoginView() {
        usernameEditText = (EditText)findViewById(R.id.usernameField);
        passwordEditText = (EditText)findViewById(R.id.passwordField);
        loginButton = (Button)findViewById(R.id.loginButton);
        signupButton = (Button)findViewById(R.id.signupButton);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                username = username.trim();
                password = password.trim();

                if (username.isEmpty() || password.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(R.string.login_error_message)
                            .setTitle(R.string.login_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    setProgressBarIndeterminateVisibility(true);

                    ParseUser.logInInBackground(username, password, new LogInCallback() {
                        @Override
                        public void done(ParseUser user, ParseException e) {
                            setProgressBarIndeterminateVisibility(false);

                            if (e == null) {
                                // Success!
                                PreserveLogIn.setPrefUserName(MainActivity.this, user.getUsername());
                                PreserveLogIn.setPrefUserId(MainActivity.this, user.getObjectId());
                                PreserveLogIn.setPrefUserFirstName(MainActivity.this, user.get("firstName").toString());
                                Intent intent = new Intent(MainActivity.this, HomeScreenActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("userName",user.getUsername());
                                intent.putExtra("userId",user.getObjectId());

                                startActivity(intent);
                            }
                            else {
                                // Fail
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage(e.getMessage())
                                        .setTitle(R.string.login_error_title)
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
                }
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void autoLogin(String userName, String userId){
        Intent intent = new Intent(MainActivity.this, HomeScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("userName",userName);
        intent.putExtra("userId",userId);
        startActivity(intent);
    }
}
