package com.example.niveditha.splitwiseproject2.EventManagement;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import ModelObjects.EventBill;
import ModelObjects.EventMember;


public class ViewReportAdapter extends ArrayAdapter {
    private final Activity context;
    protected String type;
    private List<EventMember> eventMembers = null;
    private HashMap<EventMember, Double> map = new HashMap<>();
    DecimalFormat df = new DecimalFormat("#.00");

    ViewReportAdapter(Activity context, List<EventMember> eventMembers, HashMap<EventMember,Double> map, String type) {
        super(context, R.layout.fragment_view_report_adapter, eventMembers);
        this.context = context;
        this.eventMembers = eventMembers;
        this.map = map;
        this.type = type;
    }

    static class ViewHolder {
        protected TextView title;
        protected TextView subTitle;
    }

    @Override
    public View getView(final int position, View view, ViewGroup paren) {
        ViewHolder viewHolder = null;
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.fragment_view_report_adapter, null, true);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.report_member_name);
            viewHolder.subTitle = (TextView) view.findViewById(R.id.report_amount);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        if(view!=null){
        if (eventMembers.get(position) != null) {

            final EventMember member = eventMembers.get(position);

            if(!member.getUserId().equals(PreserveLogIn.getPrefUserId(context))) {
                Double amount = map.get(member);
                if(amount<0 && type.equals("owe")){
                    viewHolder.title.setText(member.getUserName());
                    amount = amount * (-1);
                    viewHolder.subTitle.setText("you owe $"+df.format(amount).toString()+" in total");
                }
                else if(amount>0 && type.equals("owed")) {
                    viewHolder.title.setText(member.getUserName());
                    viewHolder.title.setGravity(Gravity.RIGHT);
                    viewHolder.subTitle.setGravity(Gravity.RIGHT);
                    viewHolder.subTitle.setTextColor(Color.parseColor("#FF00B72E"));
                    viewHolder.subTitle.setText("owes you $"+df.format(amount).toString()+" in total");
                }
                else{
                    viewHolder.title.setText("");
                    viewHolder.subTitle.setText("");
                }
            }
            else{
                viewHolder.title.setText("");
                viewHolder.subTitle.setText("");
            }

        }}
        return view;
    }

}