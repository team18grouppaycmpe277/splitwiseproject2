package com.example.niveditha.splitwiseproject2.UserGroupsManagement;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.GroupEvent;
import ModelObjects.JoinedEvent;
import ModelObjects.UserGroup;


public class GroupEventFragment extends Fragment implements View.OnClickListener {

    String groupId;
    String userId;
    String userGroup;
    EventAdapter adapter;
    ListView listView;
    View view;
    List<GroupEvent> groupEvents;
    List<JoinedEvent> joinEvent;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle data = getArguments();
        userId = data.getString("userId");
        userGroup = data.getString("userGroup");
        groupId = data.getString("groupId");
        View view = inflater.inflate(R.layout.fragment_group_event, container, false);
        getEvents();
        listView = (ListView) view.findViewById(R.id.event_list);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createGrp:
                // Intent intent = new Intent(this.getActivity(),CreateGroupActivity.class );
                // intent.putExtra("username", userName);
                //  startActivity(intent);
                break;
            case R.id.joinGrp:
                break;
            default:
                break;
        }
    }

    public void getEvents() {
        GroupEventFragment.this.getActivity().setProgressBarIndeterminateVisibility(true);
        ParseQuery<ParseObject> myGroup = ParseQuery.getQuery("Event");
        myGroup.whereEqualTo("grpId", groupId);
        groupEvents = new ArrayList<>();
        myGroup.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> retrivedEvents, ParseException e) {
                GroupEventFragment.this.getActivity().setProgressBarIndeterminateVisibility(false);
                if (e == null) {
                    if(!retrivedEvents.isEmpty()) {
                        for (ParseObject g : retrivedEvents) {
                            //retrived user to list of UserGroup
                            GroupEvent event = new GroupEvent();
                            event.setGrpId(g.get("grpId").toString());
                            event.setEventId(g.getObjectId().toString());
                            event.setEventName(g.get("eventName").toString());
                            event.setEventType(g.get("EventType").toString());
                            event.setEventOwner(g.get("eventOwner").toString());
                            event.setEventDate(g.get("eventDate").toString());
                            event.setEventDescription(g.get("eventDescription").toString());
                            groupEvents.add(event);

                        }
                    }
                    else{
                        CharSequence text = "No Events yet!\nCreate a new Event\nClick on add icon to create new Event";
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(GroupEventFragment.this.getActivity(), text, duration);
                        toast.show();
                    }

                     listView.setAdapter(adapter);
                     adapter = new EventAdapter(GroupEventFragment.this.getActivity(), groupEvents, userGroup);
                     listView.setAdapter(adapter);

                } else {
                    Log.d("UserGroup", "Error: " + e.getMessage());
                }
            }
        });

    }
}



