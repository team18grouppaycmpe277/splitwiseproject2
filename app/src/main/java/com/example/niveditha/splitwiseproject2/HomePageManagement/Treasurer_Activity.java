package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.GroupEvent;

public class Treasurer_Activity extends ActionBarActivity {


    ListView listview;
    List<GroupEvent> groupevent = new ArrayList<GroupEvent>();
    Treasurer_Event_List_Adapter eventAdapter;
    String currentGrpId;
    String currentGrpName;



    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.treasurer_activity);
        currentGrpId = getIntent().getExtras().get("groupId").toString();
        currentGrpName = getIntent().getExtras().get("groupName").toString();

        getEventOfGroup();
        listview = (ListView) findViewById(R.id.listViewOfEvents);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                GroupEvent groupevt = groupevent.get(position);
                Intent group_event_intent = new Intent(Treasurer_Activity.this, Treasure_Event_Members.class);
                group_event_intent.putExtra("evetId", groupevt.getEventId());
                group_event_intent.putExtra("eventName", groupevt.getEventName());
                startActivity(group_event_intent);
            }

        });

    }

        @Override
          public boolean onCreateOptionsMenu(Menu menu) {
          //Inflate the menu; this adds items to the action bar if it is present.
          getMenuInflater().inflate(R.menu.menu_treasurer_, menu);
          return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
          return true;
        }

        return super.onOptionsItemSelected(item);
        }


        public void getEventOfGroup() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.whereEqualTo("grpId",currentGrpId );

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> eventlist, ParseException e) {
                    if (e == null) {
                        for (ParseObject g : eventlist) {
                            GroupEvent events_in_group = new GroupEvent();
                            events_in_group.setEventName((String) g.get("eventName"));
                            events_in_group.setEventId(g.getObjectId());
                            groupevent.add(events_in_group);
                        }
                        //GroupEvent ge  = new GroupEvent();
                        //ge.setEventId("qwe123weq2");
                        //ge.setEventName("My Event");
                        //groupevent.add(ge);
                        eventAdapter = new Treasurer_Event_List_Adapter(Treasurer_Activity.this, groupevent);
                        listview.setAdapter(eventAdapter);
                        return;


                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }

                }
            });


        }





}