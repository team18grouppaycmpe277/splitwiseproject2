package com.example.niveditha.splitwiseproject2.UserGroupsManagement;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.niveditha.splitwiseproject2.HomePageManagement.CreateGroupActivity;
import com.example.niveditha.splitwiseproject2.HomePageManagement.UserGroupsAdapter;
import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

import ModelObjects.GroupEvent;
import ModelObjects.JoinedEvent;
import ModelObjects.UserGroup;


public class JoinedEventFragement extends Fragment implements View.OnClickListener {

    String groupId;
    String userId;
    String userGroup;
    JoinEventAdapter adapter;
    ListView listView;
    View view;
    List<GroupEvent> groupEvents;
    List<JoinedEvent> yourEvent;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle data = getArguments();
        userId = data.getString("userId");
        userGroup = data.getString("userGroup");
        groupId = data.getString("groupId");
        View view = inflater.inflate(R.layout.fragment_joined_event_fragement, container, false);
        getJoinedEvents();
        listView = (ListView) view.findViewById(R.id.join_event_list);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createGrp:
                // Intent intent = new Intent(this.getActivity(),CreateGroupActivity.class );
                // intent.putExtra("username", userName);
                //  startActivity(intent);
                break;
            case R.id.joinGrp:
                break;
            default:
                break;
        }
    }


    public void getJoinedEvents(){

        yourEvent = new ArrayList<>();
        JoinedEventFragement.this.getActivity().setProgressBarIndeterminateVisibility(true);
        ParseQuery<ParseObject> joinedEvents = ParseQuery.getQuery("Join_Event");
        joinedEvents.whereEqualTo("userId", userId);
        joinedEvents.whereEqualTo("grpId", groupId);
        joinedEvents.include("Event");
        joinedEvents.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> joinEvent, ParseException e) {
                if (e == null) {
                    for (ParseObject g : joinEvent) {
                        JoinedEvent event = new JoinedEvent();
                        event.setGrpId(g.get("grpId").toString());
                        event.setEventId(g.get("eventId").toString());
                        String s = g.get("eventId").toString();
                        event.setEventName(g.get("eventName").toString());
                        event.setUserId(userId);
                        yourEvent.add(event);
                    }

                    listView.setAdapter(adapter);
                    adapter = new JoinEventAdapter(JoinedEventFragement.this.getActivity(), yourEvent, userGroup);
                    listView.setAdapter(adapter);

                } else {
                    Log.d("coupons", "Error: " + e.getMessage());
                }
            }
        });
        JoinedEventFragement.this.getActivity().setProgressBarIndeterminateVisibility(false);
    }


}