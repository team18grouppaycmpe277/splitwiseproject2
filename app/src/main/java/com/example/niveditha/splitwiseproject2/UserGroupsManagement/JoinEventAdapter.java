package com.example.niveditha.splitwiseproject2.UserGroupsManagement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.niveditha.splitwiseproject2.EventManagement.GroupEventActivity;
import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import ModelObjects.GroupEvent;
import ModelObjects.JoinedEvent;

/**
 * Created by Vaibhav on 4/25/2015.
 */
public class JoinEventAdapter  extends ArrayAdapter {
    private final Activity context;
    private final String userGroup;
    private final List<JoinedEvent> joinEvent;
    JoinEventAdapter(Activity context, List<JoinedEvent> joinEvent, String userGroup) {
        super(context, R.layout.fragment_join_event_adapter, joinEvent);
        this.context = context;
        this.joinEvent = joinEvent;
        this.userGroup = userGroup;
    }
    static class ViewHolder {
        protected TextView title;
        protected TextView subTitle;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.fragment_join_event_adapter, null,true);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.join_event_list_name);
            viewHolder.subTitle = (TextView) view.findViewById(R.id.join_event_list_subName);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if(joinEvent.get(position) != null )
        {
            final JoinedEvent event = joinEvent.get(position);
            viewHolder.title.setText(joinEvent.get(position).getEventName());
            viewHolder.subTitle.setText(joinEvent.get(position).getEventName());
            viewHolder.title.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    onClickEvent(event.getEventId(),event.getGrpId(), event.getEventName());

                }
            });
        }

        return view;
    }

    private void onClickEvent(String eventId, String groupId, String eventName){
        Intent intent = new Intent(context, GroupEventActivity.class);
        intent.putExtra("groupId", groupId);
        intent.putExtra("userId", PreserveLogIn.getPrefUserId(context));
        intent.putExtra("eventId",eventId);
        intent.putExtra("eventName", eventName);
        context.startActivity(intent);
    }
}
