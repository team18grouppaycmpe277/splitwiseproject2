	package com.example.niveditha.splitwiseproject2.HomePageManagement;

	import android.annotation.TargetApi;

    import android.content.Intent;
    import android.support.v4.app.Fragment;
	import android.os.Build;
    import android.support.v4.app.FragmentTransaction;
    import android.support.v7.app.ActionBar.Tab;
	import android.app.ActionBar;

	import android.os.Bundle;
    import android.support.v7.app.ActionBarActivity;
    import android.view.Menu;
	import android.view.MenuItem;

    import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
    import com.example.niveditha.splitwiseproject2.R;
    import com.example.niveditha.splitwiseproject2.LogInManagement.MainActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.niveditha.splitwiseproject2.LogInManagement.MainActivity;
import com.example.niveditha.splitwiseproject2.LogInManagement.PreserveLogIn;
import com.example.niveditha.splitwiseproject2.R;
    import com.parse.ParseUser;

    import java.util.ArrayList;
import java.util.List;


    public class HomeScreenActivity extends ActionBarActivity implements android.support.v7.app.ActionBar.TabListener {

		private String[] tabs = { "Home", "Owner" , "Treasurer", "MyGroups"};
		List<Fragment> fragList = new ArrayList<Fragment>();
		String userName = "";
        String userId = "";
	   // ActionBar actionBar;

		@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		@Override
		protected void onCreate(Bundle savedInstanceState)
        {
		    super.onCreate(savedInstanceState);
		    Intent intent = getIntent();
			userName = ParseUser.getCurrentUser().getUsername();
            userId = ParseUser.getCurrentUser().getObjectId();
			setContentView(R.layout.activity_home_screen);
			ActionBar.Tab tab1, tab2, tab3, tab4;
			Fragment fragmentTab1 = new Fragment();

			android.support.v7.app.ActionBar actionBar = getSupportActionBar();
			actionBar.setHomeButtonEnabled(false);
		    actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);

			for (String tab_name : tabs) {
			  actionBar.addTab(actionBar.newTab().setText(tab_name)
					  .setTabListener(this));
			}
		}


		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.menu_home_screen, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();

			//noinspection SimplifiableIfStatement
			if (id == R.id.action_settings) {
				return true;
			}
            if(id == R.id.notifications){
               Intent intent = new Intent(HomeScreenActivity.this, NotificationActivity.class);
               startActivity(intent);
            }
			if( id == R.id.logout)
			{

                PreserveLogIn.setPrefUserId(HomeScreenActivity.this, "");
                PreserveLogIn.setPrefUserName(HomeScreenActivity.this, "");
                Intent intent = new Intent(HomeScreenActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

			}
			return super.onOptionsItemSelected(item);
		}


		@Override
		public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {

			Fragment f = null;
			Fragment tf = null;



			if (fragList.size() > tab.getPosition())
				fragList.get(tab.getPosition());


			if (f == null) {
                Bundle data = new Bundle();
                data.putString("userName",userName);
                data.putString("userId", userId);
				if(tab.getPosition() == 0) {
					tf = new HomeScreenFragment();
					tf.setArguments(data);
				}
                else if(tab.getPosition() ==1) {
                    tf = new GroupOwnerActivity();
                }
                    else if(tab.getPosition() == 2) {
                    tf = new Fragment_Treasurer_Group();
                    tf.setArguments(data);
                }
                else if(tab.getPosition() == 3) {
                    tf = new UserGroupFragment();
                    tf.setArguments(data);
                }
				else
					tf = new HomeScreenFragment();
				fragList.add(tf);
			}
			else
				tf = (HomeScreenFragment) f;

			fragmentTransaction.replace(android.R.id.content, tf);
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {

		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {

		}
	}
