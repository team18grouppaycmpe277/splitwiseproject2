        package com.example.niveditha.splitwiseproject2.HomePageManagement;

        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;

        import com.example.niveditha.splitwiseproject2.R;
        import com.parse.ParseUser;

        /**
         * author: Niveditha
         */
        public class HomeScreenFragment extends Fragment implements View.OnClickListener {

            String userName;
            String userId;
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState)
            {
                View view = inflater.inflate(R.layout.fragment_home_screen, container, false);
                userName =(String) ParseUser.getCurrentUser().get("username");

                Button createGroup = (Button) view.findViewById(R.id.createGrp);
                Button joinGroup = (Button) view.findViewById(R.id.joinGrp);
                createGroup.setOnClickListener(this);
                joinGroup.setOnClickListener(this);
                return view;
            }


            @Override
            public void onClick(View v)
            {
                switch (v.getId()) {
                    case R.id.createGrp:
                        Intent intent = new Intent(this.getActivity(),CreateGroupActivity.class );
                        intent.putExtra("username", userName);
                        intent.putExtra("userId", userId);
                        startActivity(intent);
                        break;
                    case R.id.joinGrp:
                        Intent intent2 = new Intent(this.getActivity(),JoinGroupActivity.class );
                        intent2.putExtra("username", userName);
                        startActivity(intent2);
                        break;
                    default:
                        break;
                }
            }
        }
