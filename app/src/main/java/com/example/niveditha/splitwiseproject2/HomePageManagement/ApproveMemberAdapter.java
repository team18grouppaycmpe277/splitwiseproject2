package com.example.niveditha.splitwiseproject2.HomePageManagement;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.niveditha.splitwiseproject2.R;

import java.util.List;

import ModelObjects.Friend;
import ModelObjects.Group;

/**
 * Created by Niveditha on 4/21/2015.
 */
public class ApproveMemberAdapter extends ArrayAdapter {


    private final Activity context;
    private final List<Friend> memList;
    private RadioButton mSelectedRB;
    private int mSelectedPosition = -1;
    ApproveMemberAdapter(Activity context,List<Friend> membersList){
        super(context, R.layout.pending_approval_members,membersList);
        this.context = context;
        this.memList = membersList;
    }
    static class ViewHolder {
        protected TextView text;
        protected CheckBox checkbox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.pending_approval_members, null,true);

            viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.userName1);
            viewHolder.checkbox = (CheckBox) convertView.findViewById(R.id.isJoined);
            viewHolder.checkbox.setTag(position);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.text.setText(memList.get(position).getName());
        viewHolder.checkbox.setChecked(memList.get(position).isMember());
        viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox checkbox = (CheckBox) v;
                int getPosition = (Integer) checkbox.getTag();
                memList.get(getPosition).setMember(checkbox.isChecked());
            }
        });

        return convertView;
    }
}
